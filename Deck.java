import java.util.Random;
class Deck {
	private int lastCardPos;
	private Card [] stack;
	Random rng = new Random();
	
	public Deck() {
		int [] ranks = new int [] {1,2,3,4,5,6,7,8,9,10,11,12,13};
		String [] suits = new String [] {"Hearts", "Diamonds", "Clubs", "Spades"};
		this.stack = new Card [52];
		lastCardPos = this.stack.length-1;
		int i = 0;
		for(int j = 0; j<ranks.length; j++) {
			for(int k = 0; k<suits.length; k++) {
				this.stack[i] = new Card (ranks[j], suits[k]);
				i++;
			}
		}
	}
	
	public int getLength() {
		return this.lastCardPos;
	}
	
	public Card drawTopCard() {
		Card toReturn = null;
		if(lastCardPos>=0){
			toReturn=this.stack[lastCardPos];
			this.lastCardPos--;
		}
		return toReturn;
	}
	
	public String toString() {
		String s = "";
		for(Card c : this.stack) {
			s+= c.toString() + "\n";
		}
		return s;
	}
	
	public void shuffle() {
		for(int i = 0; i<this.stack.length; i++) {
			int ranNum = rng.nextInt(stack.length);
			Card cardSwtich = this.stack[i];
			this.stack[i] = this.stack[ranNum];
			this.stack[ranNum] = cardSwtich;
		}
	}
}