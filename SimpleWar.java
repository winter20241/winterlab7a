public class SimpleWar {
	public static void main(String [] args) {
		Deck pile = new Deck();
		pile.shuffle();
		
		int p1Points = 0;
		int p2Points = 0;
		
		int numOfCards = pile.getLength();
		int round = 1;
		while(numOfCards>0) {
			
			Card p1Card;
			p1Card = pile.drawTopCard();
			double p1Score;
			p1Score = p1Card.calculateScore();
			
			Card p2Card;
			p2Card = pile.drawTopCard();
			double p2Score;
			p2Score = p2Card.calculateScore();
			
			String winner = "";
			if(p1Score>p2Score){
				winner = "Player 1";
				p1Points++;
			}else {
				winner = "Player 2";
				p2Points++;
			}
			System.out.println("Round: " + round + "\nPlayer 1's card: " + p1Card + "\nPlayer 2's card: " + p2Card);
			System.out.println(winner + " wins this round");
			System.out.println("Player 1 points: " + p1Points + "\nPlayer 2 points: " + p2Points + "\n");
			numOfCards = pile.getLength();
			round++;
		}
		
		if(p1Points>p2Points) {
			System.out.println("Player 1 wins the game");
		}else {
			System.out.println("Player 2 wins the game");
		}
	}
}