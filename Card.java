class Card {
	private int rank;
	private String suit;
	
	public Card(int rank, String suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public String getSuit() {
		return this.suit;
	}
	
	public String toString() {
		String printRank = Integer.toString(rank);
		if(rank == 1) {
			printRank = "Ace";
		}else if(rank == 11) {
			printRank = "Jack";
		} else if (rank == 12) {
			printRank = "Queen";
		}else if (rank == 13) {
			printRank = "King";
		}
		return printRank + " of " + suit;
	}
	
	public double calculateScore() {
		double score = 0;
		score = score + rank;
		if(suit.equals("Heart")){
			score = score + 0.4;
		}else if(suit.equals("spades")){
			score = score + 0.3;
		}else if(suit.equals("Diamonds")) {
			score = score + 0.2;
		}else {
			score = score + 0.1;
		}
		return score;
	}
}